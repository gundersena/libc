#ifndef _HTTP_H_
#define _HTTP_H_

#define PORT "3000"

typedef struct
{
    char type[100];
    char path[200];
}
Req;

typedef struct
{
    int status;
    char* content_type;
    int content_length;
    char* body;
}
Res;

/* User defined handlers must be this shape */
typedef void (*Handler)(Req* req, Res* res);

/* Add an api endpoint */
void use(char* path, Handler handler);

/* Start the server */
int http_start(char* port);

#endif
