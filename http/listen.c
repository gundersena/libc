/**
 * listen.c - Create and bind a server socket to a port
 * 
 */

#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>

#include "listen.h"

#define QUEUE 8


int init_server_socket(char* port)
{
    int fd, yes = 1;
    struct addrinfo hints, *servinfo, *p;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, port, &hints, &servinfo);

    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            continue;
        }

        if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            close(fd);
            freeaddrinfo(servinfo);
            return -1;
        }

        if (bind(fd, p->ai_addr, p->ai_addrlen) == -1) 
        {
            close(fd);
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    if (p == NULL) { return -1; }

    if (listen(fd, QUEUE) == -1)
    {
        close(fd);
        return -1;
    }

    return fd;
}