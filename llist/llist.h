#ifndef _LLIST_H_
#define _LLIST_H_

typedef struct 
{
	struct LLNodeT* head;
	int entries;
}
LL;

typedef int (*LLCb)(void*, void*);

LL* ll_create(void);

int ll_push(LL* ll, void* data);

int ll_append(LL* ll, void* data);

void* ll_search(LL* ll, LLCb cb, void* arg);

void* ll_delete(LL* ll, LLCb cb, void* arg);

int ll_loop(LL* ll, LLCb cb, void* arg); 

void ll_free(LL* ll);

#endif