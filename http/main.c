/**
 * main.c - HTTP server that uses http.c framework
 * 
 * curl http://localhost:3000/api/login
 * 
 */

#include <stdio.h>
#include <string.h>

#include "http.h"

void login(Req* req, Res* res)
{
    (void) req;

    char* content = "Login works!\n";

    res->status = 200;
    res->content_length = strlen(content);
    res->content_type = "text/html";
    res->body = content;
}

void signup(Req* req, Res* res)
{
    (void) req;

    char* content = "Sign Up works!\n";

    res->status = 200;
    res->content_length = strlen(content);
    res->content_type = "text/html";
    res->body = content;
}

/**
 * Register the endpoints and start the server.
 */
int main(void)
{
    puts("Registering endpoints");

    use("/signup", &signup);
    use("/login", &login);

    http_start(PORT);
}
