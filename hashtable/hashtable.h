#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

#include "llist.h"

typedef struct
{
    int size;
    int entries;
    LL** store;
}
HT;

HT* ht_create(void);

int ht_put(HT* ht, char* key, void* data);

void* ht_get(HT* ht, char* key);

void* ht_delete(HT* ht, char* key);

int ht_free(HT* ht);

#endif
