#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "llist.h"

typedef struct
{
    char* name;
    char* team;
    int age;
}
Player;

Player* create_player(char* name, char* team, int age)
{
    Player* p = malloc(sizeof(*p));
    p->team = strdup(team);
    p->name = strdup(name);
    p->age = age;
    return p;
}

int find_player(void* data, void* arg)
{
    Player* player = data;
    return strcmp(player->name, (char*) arg);
}

int change_team(void* data, void* arg)
{
    Player* player = data;
    player->team = (char*) arg;
    return 0;
}

int main(void)
{
    puts("Creating new linked list...");

    LL* ll = ll_create();

    puts("Pushing some data...");

    Player* lebron = create_player("Lebron James", "Lakers", 37);
    Player* giannis = create_player("Giannis Antetokounmpo", "Bucks", 27);

    ll_push(ll, lebron);
    ll_push(ll, giannis);

    puts("Finding the data that was pushed...");

    assert(ll_search(ll, find_player, "Lebron James") == lebron);

    puts("Appending some data...");

    Player* kd = create_player("Kevin Durant", "Nets", 33);

    ll_append(ll, kd);

    puts("Finding the data that was appended...");

    assert(ll_search(ll, find_player, "Kevin Durant") == kd);

    puts("Looping over linked list and applying change...");

    ll_loop(ll, change_team, "Bucks");

    Player* kd_ref = ll_search(ll, find_player, "Kevin Durant");

    assert(strcmp(kd_ref->team, "Bucks") == 0);

    puts("Deleting data from linked list...");

    ll_delete(ll, find_player, "Lebron James");

    assert(ll_search(ll, find_player, "Lebron James") == NULL);

    puts("Tests complete!");

    return 0;
}