#include <stdio.h>
#include <assert.h>

#include "hashtable.h"

int main(void)
{
    puts("Creating new hashtable...");

    HT* ht = ht_create();

    char* login_data = "This is the login data!";
    char* register_data = "This is the register data!";

    puts("Putting in some data...");

    ht_put(ht, "/login", login_data);
    ht_put(ht, "/register", register_data);

    puts("Getting values and asserting they are correct...");

    assert(login_data == ht_get(ht, "/login"));
    assert(register_data == ht_get(ht, "/register"));

    puts("Checking that delete works...");

    ht_delete(ht, "/login");

    assert(ht_get(ht, "/login") == NULL);

    puts("Tests complete!");

    return 0;
}